
### Introduction to High-throughput Sequencing (3/11/2022)
  - [High-Throughput Sequencing Theory](rnaseq_bioinformatics/1_hts_background.pdf)
  - [Bioinformatics Overview](rnaseq_bioinformatics/3_bioinformatics_overview.pdf)
  - [Quality Score Primer](rnaseq_bioinformatics/notebooks/quality_scores.md)
  
### Bioinformatics for RNA-seq (3/18/2022)
  - Computing Environment
    - [Introduction to Open OnDemand](rnaseq_bioinformatics/ondemand_for_rnaseq.md)
    - [Cloning the Gitlab Repository](../misc/git_cloning.md)
    - Bash Intro
      - [Setup for SWC Unix Lessons](misc/unix_shell_setup.Rmd)
      - [Software Carpentry: The Unix Shell](https://swcarpentry.github.io/shell-novice/)
  - RNA-Seq Bioinformatic Steps
    - [Quality Control](rnaseq_bioinformatics/notebooks/fastqc.Rmd)
    - [Trimming and Filtering](rnaseq_bioinformatics/notebooks/fastq_trimming.Rmd)
    - [Download and Index Genome](rnaseq_bioinformatics/notebooks/genome_prep.Rmd)
    - [Mapping Reads to a Reference Genome](rnaseq_bioinformatics/notebooks/mapping.Rmd)
    - Appendix
      - [Make Adapter File](rnaseq_bioinformatics/notebooks/make_adapter_fasta.Rmd)
      - [Quality Score Explanation](rnaseq_bioinformatics/notebooks/quality_scores.Rmd)
      - [Run Full Pipeline](rnaseq_bioinformatics/notebooks/run_everything.Rmd)

### Statistical Analysis for RNA-seq  (3/25/2022)
  - RNA-seq Statistical Analysis Table of Contents


### Microbiome Analysis (4/1/2022)
  - Microbiome Table of Contents
   <!-- a normal html comment 
  (microbiome/microbiome_toc.md)
  -->

### Bioinformatics for scRNA-seq (4/8/2022)
 <!-- a normal html comment 
  - [scRNA-Seq TOC](sc_rnaseq/sc_rnaseq_toc.md)
-->
  - Background Slides
  - Bioinformatic Analysis: PBMC 1k
  - Statistical Analysis Slides
  - Statistical Analysis

#### scRNA-seq Appendix
  - Configuration File
  - Download Data

### Bioinformatics for Flow Cytometry  (4/15/2022)
  - Flow Cytometry Table of Contents

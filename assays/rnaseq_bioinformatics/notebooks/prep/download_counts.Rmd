---
title: "Setup for RNA-Seq"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
library(here)
library(dplyr)
here("assays/rnaseq_bioinformatics/notebooks/config.R") %>%
  source
```

```{bash}
set -u
ddsclient list -p HTS_course > $OUT_DIR/dds_full_list.txt
```

```{bash}
set -u
grep 2019 $OUT_DIR/dds_full_list.txt | grep -v fastq
```

```{bash}
set -u
chmod -R u+w $DATA_DIR
ddsclient download -p HTS_course --include hts_2019_data/hts2019_pilot_counts $DATA_DIR
chmod -R a-w $DATA_DIR
```


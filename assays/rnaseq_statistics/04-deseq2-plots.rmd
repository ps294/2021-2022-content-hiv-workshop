---
title: An R Markdown document converted from "/hpc/home/ts415/project_repos/hiv_r25/hiv-workshop-2020-2021/assays/rnaseq_statistics/04-deseq2-plots.ipynb"
output: html_document
---

# Generate figures from DESEq2 analysis

## Load data and generate DE results

```{r}
library(tidyverse)
library(DESeq2)
library(dendextend)
library(RColorBrewer)
```

```{r}
imgdir <- "Data/"

imgfile <- file.path(imgdir, "HIVdemo2021-DDS.RDS")

imgfile

tools::md5sum(imgfile)

ddsadd <- readRDS(imgfile)
```

```{r}

### Estimate Size Factors
ddsadd <- estimateSizeFactors(ddsadd)
### Estimate Dispersion parameters (for each gene)
ddsadd <- estimateDispersions(ddsadd)
### Fit NB MLE model
ddsadd <- DESeq(ddsadd)
```

## Dot Plots

```{r}
### Merge gene expression with meta data
myDEplotData <- function(mydds, geneid, mergelab) {
    counts(mydds, normalize = TRUE) %>%
        as_tibble(rownames="gene") %>%
        filter(gene == geneid) %>%
        gather(Label, geneexp, -gene) %>%
        select(-gene) -> genedat

    colData(mydds) %>%
        as.data.frame %>%
        as_tibble %>%
        full_join(genedat, by = mergelab) -> genedat
    
    return(genedat)
}

### Plot using a single factor
myDEplot0 <- function(mydds, geneid, grpvar, mergelab) {
    mydat <- myDEplotData(mydds, geneid, mergelab)
    ggplot(mydat, aes_string(x=grpvar, y = "geneexp", col = grpvar))+ geom_point()
}


### Allow for coloring with respect to another factor
myDEplot <- function(mydds, geneid, grpvar, colvar, mergelab) {
    mydat <- myDEplotData(mydds, geneid, mergelab)
    ggplot(mydat, aes_string(x=grpvar, y = "geneexp", col = colvar))+ geom_point()
}
```

```{r}
### Stratify by condition color by genotype
myDEplot(ddsadd, "CNAG_00003", "condition", "genotype", "Label")
```

```{r}
### Stratify by genotype color by conditon
myDEplot(ddsadd, "CNAG_00003", "genotype", "condition", "Label")
```

```{r}
### Stratify by genotype
myDEplot0(ddsadd, "CNAG_00003", "genotype", "Label")
```

## Volcano plots

```{r}
### Volcano plot for condition effect
ggplot(results(ddsadd, contrast = c("condition", "pH4", "pH8"), tidy = TRUE), 
       aes(x = log2FoldChange, y = -log10(padj))) + geom_point()
```

## 

## Get "expression" data from counts

```{r}
### Normalized counts
normexp <- assay(ddsadd, normalize=TRUE)
class(normexp)
```

```{r}
### rlog transform
rlexp <- rlog(ddsadd, blind = TRUE)
class(rlexp)
```

```{r}
vstexp <- vst(ddsadd)
class(vstexp)
```

```{r}
### FPM
fpmexp <- fpm(ddsadd)
class(normexp)
```

## Principal Components Analysis

```{r}
###  Color by condition
plotPCA(rlexp, intgroup = c("condition"))
```

```{r}
### Color by condition and genotype using rlog and then vst transformed counts
plotPCA(rlexp, intgroup = c("condition", "genotype"))
plotPCA(vstexp, intgroup = c("condition", "genotype"))
```

## Clustering

```{r}
### Simple dendogram with no annotation
options(repr.plot.width = 9, repr.plot.height = 5)
dists <- dist(t(assay(rlexp)))
plot(hclust(dists, metho = "complete")) 
```

```{r}
### Create dendogram object
assay(rlexp) %>%
    t() %>%
        dist %>%
            hclust(method = "complete") %>%
                as.dendrogram -> mydend
```

```{r}
### Plot dendogram object
plot(mydend)
```

```{r}
### Function to annotate dendogram object
dendplot <- function(mydend, columndata, labvar, colvar, pchvar) {
    cols <- factor(columndata[[colvar]][order.dendrogram(mydend)])
    collab <- brewer.pal(max(3,nlevels(cols)),"Set1")[cols]
    pchs <- factor(columndata[[pchvar]][order.dendrogram(mydend)])
    pchlab <- seq_len(nlevels(pchs))[pchs]
    lablab <- columndata[[labvar]][order.dendrogram(mydend)]
    
    mydend %>% 
        set("labels_cex",1) %>% 
        set("labels_col",collab) %>%
        set("leaves_pch",pchlab) %>%
        set("labels", lablab)
}
```

```{r}
### Annotate by condition only
dendplot(mydend, ddsadd@colData, 
         "condition",    # variable that show in label
         "condition",    # variable that define color
         "condition") %>% # variable that define shape of points
    plot
```

```{r}
### label by condition color by genotype
dendplot(mydend, ddsadd@colData, 
         "condition",    # variabled that show in label
         "genotype",    # variable that define color
         "condition") %>% # variable that define shape of points
    plot
```

```{r}
### label by genotype  color by condition
dendplot(mydend, ddsadd@colData, 
         "genotype",    # variable that show in label
         "condition",    # variable that define color
         "condition") %>% # variable that define shape of points
    plot
```

```{r}
sessionInfo()
```


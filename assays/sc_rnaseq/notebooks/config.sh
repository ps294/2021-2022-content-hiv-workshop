# ---
# jupyter:
#   jupytext:
#     formats: sh:light
#     text_representation:
#       extension: .sh
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.11.1
#   kernelspec:
#     display_name: Bash
#     language: bash
#     name: bash
# ---

# +
set -u
DATA_DIR="/data"
SCRNASEQ_DATA_DIR="$DATA_DIR/scrna_seq"

PBMC3K_DATA_DIR="$SCRNASEQ_DATA_DIR/pbmc3k_data"
PBMC1K_DATA_DIR="$SCRNASEQ_DATA_DIR/pbmc1k_data"

PBMC3K_FASTQ_DIR="$PBMC3K_DATA_DIR/pbmc3k_fastqs"
PBMC1K_FASTQ_DIR="$PBMC1K_DATA_DIR/pbmc_1k_v3_fastqs"
GENOME_BASE="$SCRNASEQ_DATA_DIR/genome_dir"

# GENOME_DIR="$SCRNASEQ_DATA_DIR/genome_dir/refdata-cellranger-GRCh38-3.0.0"
GENOME_DIR="$GENOME_BASE/refdata-gex-GRCh38-2020-A"

# RAW_10X_DATA_DIR="$PBMC3K_DATA_DIR/pbmc2700_10x_raw"
# PROCESSED_10X_DATA_DIR="$PBMC3K_DATA_DIR/pbmc2700_10x_processed"

mkdir -p $PBMC3K_DATA_DIR $PBMC1K_DATA_DIR $GENOME_DIR


SCRATCH_DIR="$HOME/work/scratch"
OUTDIR="$SCRATCH_DIR/output"
mkdir -p $OUTDIR
